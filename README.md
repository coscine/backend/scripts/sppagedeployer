## Executing the SPPageDeployer

The SPPageDeployer uses one single input argument (`--ref` or `--r`) that is a GitLab reference string. This reference string can be a `tag`, `branch name` or a `commit id`.

Example:
```
...\Coscine.SPPageDeployer.exe --ref 'master'
```
Should the `--ref` input argument be omitted, then a default value `"master"` will be set.

## C# Template

This template includes:

* Automatic building using cake
* Automatic testing with NUnit
* Automatic linting with Resharper
* Automatic documentation publishing using Gitlab CI / CD and a self written script which puts the docs in the docs folder to the wiki
* Automatic releases using semantic-release ([ESLint Code Convention](docs/ESLintConvention)), cake and Gitlab CI / CD

## What you need to do

Place you C# project solution file in .src/.
Make sure Create directory for solution is unticked.

![alt text](docs/images/create_project.png "Create a new Project")

Delete unused docs and update this README.

Add [NUnit](docs/nunit.md) tests to your solution.

## Building

Build this project by running either the build.ps1 or the build<span></span>.sh script.
The project will be build and tested.

### Links 

*  [Commit convention](docs/ESLintConvention.md)
*  [Everything possible with markup](docs/testdoc.md)
*  [Adding NUnit tests](docs/nunit.md)
