using Coscine.Action.Utils;
using Coscine.Configuration;
using Coscine.Database.Models;
using Coscine.Database.Settings;
using NDesk.Options;
using System.Collections.Generic;
using LinqToDB.Data;
using System;

namespace Coscine.SPPageDeployer
{
    public class Program
    {
        private static string branchName = "master";

        public static void Main(string[] args)
        {
            var optionSet = new OptionSet() {
                { "r|ref=",  "Branch or Tag name to grab the Templates from. It defaults to the master branch.",
                   x => branchName = x },
            };

            List<string> extra;
            try
            {
                extra = optionSet.Parse(args);
            }
            catch (OptionException e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Try '--help' for more information.");
            }

            var configuration = new ConsulConfiguration();
            var currentTemplateBranchForRedeploy = configuration.GetStringAndWait("coscine/scripts/SPPageDeployer/current_template_branch");
            if (branchName != currentTemplateBranchForRedeploy)
            {
                Console.WriteLine($"Installing templates with git reference {branchName}");
                try
                {
                    if (configuration.Put("coscine/scripts/SPPageDeployer/current_template_branch", branchName) && configuration.Put("coscine/local/sharepoint/pagebranch", branchName))
                    {
                        Console.WriteLine("Added template key to Consul");
                        DataConnection.DefaultSettings = new CoscineSettings(configuration);
                        using (var pageUtil = new PageUtil(configuration))
                        {
                            pageUtil.DeployRootPages();
                            Console.WriteLine("Deployed root pages");
                            var projectModel = new ProjectModel();
                            foreach (var project in projectModel.GetAll())
                            {
                                try
                                {
                                    pageUtil.DeployProjectPages(project);
                                    Console.WriteLine($"Deployed pages for project {project.Slug}");
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine($"{e.Message}");
                                    Console.WriteLine($"Error: Could not deploy pages for project {project.Slug}");
                                }
                            }
                        }
                    }
                    // Not handling 'else' scenario
                }
                catch (Exception e)
                {
                    Console.WriteLine($"{e.Message}");
                    Console.WriteLine($"Error: Could not add keys to Consul");
                }
            }
            else
            {
                Console.WriteLine($"Deployment of templates skipped. Correct versions are installed already.");
            }
        }
    }
}
